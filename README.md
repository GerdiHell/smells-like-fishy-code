# What is this about?

Smells like fishy code is a open source knowledge base. It is counting on your contribution and will help us all to get even better. Everyone can use this space to spread their knowledge or make existing content better. For this you can jsut 

# How does it work?

This page is build with docsify (https://docsify.js.org/#/). Docsify is a documentation generator. It is using simple readme files and build a beautiful webpage out of it. Super fast. So we can focus on the content.

# Run this website locally

- clone/fork this repo
- use the terminal
- install docsify: `npm i docsify-cli -g`
- go to the project root
- run: `docsify serve blog`
- open http://localhost:3000
- grab a beer and enjoy yourself (i suggest https://crewrepublic.de/index.php?id_product=25&controller=product. It is really amazing)

