[cat]: ./assets/cat_keyboard.gif "cat with keyboard"
[monster]: ./assets/monster.gif "monster which is on a riot"

# Introduction

This is the homepage about the nasty things that you will face during your everyday working. Sometime they creep silent and hidden in your code just to shock you with some super unpredictable behavior or side effects. And not just that. It doesn't has to be that obvious. There are countless ways to notice this beasts after a long time being totally unknown in your code. Often exactly then when you need to modify some existing code. Yes, it is all about these scary little creatures out there called **Code Smells**.

![Cat with a keyboard.][cat]

## Basic structure of this site

But first of all i will explain the basic structure of this website. There is a lot of text and i know that we are lazy. For this i added always an TL:DR section (if it is necessary) at the end of an section. This helps you to get as much information as possible without reading all of it. And there will be a lot of text. You're welcome. Btw. This site has an offline functionality. (such progressive...) So feel free to use it on your phone and open it again when there is no internet connection.

> **TL;DR**
>
> This is a test. Now you are used to this little box. Remember it.

## Under construction / Contribute!

Feel free to help me fill this page with content! Just use this repository and start modifying it. If you find mistakes or have some good ideas to make this page better -> pls do it. Maybe we can use this page as a basis to build a knowledge base for code quality. We will see. Nevertheless i am looking forward for some feedback. Thx guys.

## What is a code smell?

A Code Smell is a sign that something is fishy about the quality of the code. This problem should be fixed by refactoring. The problem with a code smell is that the code is not really broken. You realize broken code. Immediately. But code smells... Not that easy. The chance that e.g the next change destroys something is high. So code smells are more like ticking time bombs. Theoretically they can live unnoticed for years in a well running system.

> **TL;DR:**
>
> Code smell = indicator for bad code quality and a ticking time bomb

## Why should i care?

So why should you care? I mean the system runs and everything is fine.

Ha! Not even close.

There are several reasons why you should take care about avoiding or refactoring code smells. Here are some interesting insights of a [study](https://stripe.com/files/reports/the-developer-coefficient.pdf) by stripe of the year 2018 as an example.

- How many hours per week do you estimate developers at your company waste on maintenance (i.e. dealing with bad code / errors, debugging, refactoring, modifying)? - 17.3 hours

- How many hours each week do you think the average developer at your company spends on addressing “technical debt?” - 13,5 hours

Code smells can be very expensive. They block us in our everyday work and most important (of course) it doesn't make fun to deal with code that is e.g. not understandable, readable, confusing in general or just not stable. We should also care about code smells because of our colleagues. We don't want other people to fight with our code. We want them to work with our stuff easily. Just imagine that you are on a sick leave and they have to call you because no ones understands your code... horrible. So it is also in your interest to write stuff that will not implode if someone touches it.

> **TL;DR:**
>
> You should care. Period.

## What can i do?

[Refactoring](https://refactoring.com/) is the magic word. I am not talking about daily 8 hour sessions. Instead you should follow the camper rule: Leave the place healthier than when you found it. If you run into a code smell it is a good idea to refactor it immediately. Of course it depends on how big the issue is and if you have to deal with the code. But in general we should always improve code. The code base is a living organism. It evolves over time. And if you are aware of code smells and clean code there is a good chance that this organism will not transform into an not uncontrollable monster.

![Monster.][monster]

> **TL;DR:**
>
> Code evolves. Refactoring helps to keep the code clean and not smelling.

## What is clean code?

To notice code smells it is necessary to know how not smelling code looks like. I am talking about clean code. One of the most famous books on this planet when it comes to code quality. Of course you don't need to have the same opinion as the author. But to be honest, many things that are written down in clean code are pretty helpful. Clean code is not part of this topic but we will not go anywhere without some basics. Check <a href="#/content/cleanCode">Clean Code</a> for a little reminder.

> **TL;DR:**
>
> clean code = not smelling code

## Last side note

Most of the code smells discussed on this site are regarding object oriented programming. 
