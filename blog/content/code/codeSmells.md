[smellcat]: ./assets/cat_smells.gif "cat which is smelling a sock"

# Code Smells

Now we are getting to the good stuff. On this page we are diving into the ugly world of code smells. Make yourself comfortable, grab a cup of coffee and enjoy the ride.

![Cat which sniffs on a sock.][smellcat]

_Smells. Smells everywhere._

---

## Mysterious Name

One of the most important things to have understandable code are clear and fitting names. Names for variables, functions, methods, classes and modules need to be self-explanatory. This is one of the key principles of clean code and (surprise) a code smell if this is not the case.

Many people seems to think that it is not worth the time to refactor names. Of course it can be dangerous to simply rename something. The dependencies can be a problem. But on the one hand the right naming can spare a lot of time for developers after you. On the other hand if you are not able to find a simple name for e.g. your function than there is maybe a deeper design issue inside of this function to care about. An IDE like Visual Studio Code or PHPStorm makes it easy to rename things.

The rules for naming things right is a part of clean code. I will add some examples for this as well on this page.

## Duplicated Code

**Contra:**

- it takes time to check the supposed to be duplicated code (often there a slightly difference which are hard to find)
- hard to manage changes (same changes for every duplicated code)

**Identifier:**

- big functions
- same functionality in different places

**Approach**

1. Extract Function

Smelly

```
function printOwing() {
  $this->printBanner();

  // Print details.
  print("name:  " . $this->name);
  print("amount " . $this->getOutstanding());
}
```

Not that smelly anymore

```
function printOwing() {
  $this->printBanner();
  $this->printDetails($this->getOutstanding());
}

function printDetails($outstanding) {
  print("name:  " . $this->name);
  print("amount " . $outstanding);
}
```

## Long Function

**Contra**

- hard to understand
- could be a sign that the function does more than it should (SOLID)
- possible reusability of code is missed

**Identifier**

- if you want to make a comment -> make a new function
- to many parameters
- big loops

**Approach**

Smelly

```
function calculateAndSaveOrder($products, $customerName, $isCustomerPremiumMember, $orderDate) {

  //price calculation
  $totalPrice = 0;

  foreach($products as $product){
     $totalPrice += $product->getPrice();
  }

  //freight costs calculation
  if($isCustomerPremiumMember){
      $freightCosts = $totalPrice * 0.20;
  } else {
    $freightCosts = $totalPrice * 0.30;
  }

  //save order
  $orderSaver = $this->getOrderSaver();
  $this->getOrderSaver()$orderSaver->saveOrderForCustomer($customerName, $products, $totalPrice, $freightCosts, $orderDate);

}
```

Not that smelly anymore

```
function calculateOrderTotals( $products): int {
  $totalPrice = 0;
  foreach($products as $product){
     $totalPrice += $product->getPrice();
  }
  return $totalPrice;
}

function calculateFreightCosts(bool $isPremiumMember, int $orderTotals): int{
  return isPremiumMember ? $orderTotal*0.20 : $orderTotal*0.30
}

function saveOrderForCustomer(string $customerName, array $products, int $totalPrice, int $freightCosts): void{
  $this->getOrderSaver->saveOrderForCustomer($customerName, $products, $totalPrice, $freightCosts);
}

function processOrder(string $customerName, array $products, int $totalPrice, int $freightCosts): void{
  $orderTotals = $this->function calculateOrderTotals( $products);
  $freightCosts = $this->function calculateFreightCosts(bool $isPremiumMember, int $orderTotals);
  $this->saveOrderForCustomer(string $customerName, array $products, int $totalPrice, int $freightCosts)
}
```

## Long Parameter List

**Contra**

- could be a sign that the function does more than it should (SOLID)
- confusing

**Identifier**

- ...a long list of parameters

**Approach**

Smelly

```
function calculateOrderTotals( $products): int {
  $totalPrice = 0;
  foreach($products as $product){
     $totalPrice += $product->getPrice();
  }
  return $totalPrice;
}

function calculateFreightCosts(bool $isPremiumMember, int $orderTotals): int{
  return isPremiumMember ? $orderTotal*0.20 : $orderTotal*0.30
}

function saveOrderForCustomer(string $customerName, array $products, int $totalPrice, int $freightCosts): void{
  $this->getOrderSaver->saveOrderForCustomer($customerName, $products, $totalPrice, $freightCosts);
}

function processOrder(string $customerName, array $products, int $totalPrice, int $freightCosts): void{
  $orderTotals = $this->function calculateOrderTotals( $products);
  $freightCosts = $this->function calculateFreightCosts(bool $isPremiumMember, int $orderTotals);
  $this->saveOrderForCustomer(string $customerName, array $products, int $totalPrice, int $freightCosts)
}
```

Not that smelly anymore

```
function calculateOrderTotals($products): int {
  $totalPrice = 0;
  foreach($products as $product){
     $totalPrice += $product->getPrice();
  }
  return $totalPrice;
}

function calculateFreightCosts(Order $order): int{
  return $order->isPremiumMember() ? $orderTotal*0.20 : $orderTotal*0.30
}

function saveOrderForCustomer(Order $order): void{
  $this->getOrderSaver->saveOrderForCustomer($order->getCustomerName(), $order->getProducts(), $order->getTotalPrice(), $order->getFreightCosts());
}

function processOrder(Order $order): void{
  $orderTotals = $this->function calculateOrderTotals($order->getProducts());
  $freightCosts = $this->function calculateFreightCosts($order);
  $this->saveOrderForCustomer($order);
}
```

## Global Variables

**Contra**

- can be modified from anywhere
- unpredictable behavior
- hard to comprehend changes on global data

**Identifier**

- no restriction in changing this variable on a global level

**Approach**

- just do not
- restrict the accessibility to this variable
- just do not
- example will be added here

## Mutable Data

**Contra**

- not predictable
- surprising side effects
- hard to track down where changes happen (endless debug sessions)

**Identifier**

- working with the reference of an object with a big scope

**Approach**

Approach will follow

## Divergent Change

## Shotgun Surgery

## Feature Envy

## Data Clumps

## Primitive Obsession

## Repeated Switches

## Loops

## Lazy Element

## Speculative Generality

## Temporary Field

## Message Chains

## Middle Man

## Inside Trading

## Large Class

## Alternative Classes with different Interfaces

## Data Class

## Refused Bequest

## Comments


