# Tools

It would not be an IT related topic if there are not a bunch of useful tools to help. On this site i am collecting tools regarding
 code quality.

 ## Code Sniffer

 - https://github.com/squizlabs/PHP_CodeSniffer

 ## Testing

 - https://jestjs.io/
 - https://codeception.com/

 ## Static code analysis

 - https://github.com/phpstan/phpstan




