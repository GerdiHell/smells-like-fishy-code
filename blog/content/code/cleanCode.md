[cleancat]: ./assets/cat_clean.gif "cat which is cleaning"

# Clean Code

Yes. I know. Clean code. We know the buzzword. And somehow we know what it means. BUT it is important to understand clean code to be more aware of code smells. This page will be extended with a more detailed description in the future. But for now this is a general overview about the characteristics of clean code. In a super small nutshell.

![cat which is cleaning][cleancat]

>“You know you are working on clean code when each routine you read turns out to be pretty much what you expected. You can call it beautiful code when the code also makes it look like the language was made for the problem.”
— Ward Cunningham

## 1. Simple

Simple does not mean that there can't be a high level of complexity. Simple means here that the solution is as simple as possible and is as easy as possible to follow.

## 2. Readable

Coding is not about communication with the computer it is about communicating with human beings. Of course the written code needs to be interpreted by the machine in the end to produce the results that we need. And even the worst readable code could be understandable for a machine. But it will take years to modify it. We are working with this code every day. We need to write , modify and therefore understand it. If code is not understandable to us we will have problems to work with it. Sounds pretty obvious, doesn't it? But it seems like we forget this from time to time.

> “Any fool can write code that a computer can understand. Good programmers write code that humans can understand.” —Martin Fowler

We often try to reduce lines of code. Interesting is: **If is code is not readable to a human being it can be a sign that the code is full of code smells.**

> TL:DR
>
> Code is readable by a human being.

## 3. Considerate

Clean code respects the time of your colleagues. It is written with the other guys in mind who need to work with your code someday. 

## 4. Tested

Hands down. Not tested code CAN NOT BE clean code. It is not reliable. It will never be. We all know it. When someone is changing something without testing it properly everything explodes. Literally.

## 5. Practiced

No one knows how to produce clean code by just reading about it. It is about experience and practice. You are getting better and better over the time. And the most important thing: you will change your mindset. A sense for quality will evolve. 

## 6. Constantly Refactored

Code should be constantly getting refactored. It is important to care about the code quality. This will prevent some bad surprises when it is to late.

## 7. SOLID

### **S**ingle responsibility principle

A class or module should have one, and only one, reason to be changed.

### **O**pen/closed principle

Software entities (classes, modules, functions, etc.) should be open for extensions, but closed for modification.

### **L**iskov substitution principle

Liskov substitution principle states that if S is a subtype of T, then objects of type T may be replaced (or substituted) with objects of type S.

### **I**nterface segregation principle

Do not add additional functionality to an existing interface by adding new methods.
Instead, create a new interface and let your class implement multiple interfaces if needed.

### **D**ependency inversion principle

It's a way to decouple software modules. Classes should depend on abstraction but not on concretion. Let me drop here a quote:

> A. High level modules should not depend upon low level modules. Both should depend upon abstractions.
>
> B. Abstractions should not depend upon details. Details should depend upon abstractions.
>
> Robert C. Martin
