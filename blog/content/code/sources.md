# Sources

* https://medium.com/s/story/reflections-on-clean-code-8c9b683277ca

* https://martinfowler.com/books/refactoring.html

* https://sourcemaking.com/

* https://codeburst.io/write-clean-code-and-get-rid-of-code-smells-aea271f30318

* https://www.oreilly.com/library/view/clean-code/9780136083238/

* https://guide.freecodecamp.org/

* https://www.sihui.io/code-smell-cheat-sheet/






