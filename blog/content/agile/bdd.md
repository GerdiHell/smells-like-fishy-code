[mindBlown]: ./assets/mind_blown.gif "a guy whos mind is blown."


# Behavior Driven Design

## What is BDD?

BDD is short for Behavior Driven Design. 

![a guy whos mind is blown][mindBlown]

BDD is a software development methodology. It is directly connected with agile practices. The basic idea of Behavior Driven Design is to create a bridge between IT and business. BDD helps to communicate requirements regarding software products easier and with higher precision. BDD is like a language for EVERY team member who is involved in the development process. Devs, project managers AND even the stakeholder on the customer side. Here is a quick summary from https://codeception.com/docs/07-BDD:

>The idea of story BDD can be narrowed to:
>
>- describe features in a scenario with a formal text
>- use examples to make abstract things concrete
>- implement each step of a scenario for testing
>- write actual code implementing the feature
>- By writing every feature in User Story format that is automatically executable as a test we ensure that: business, developers, QAs and managers are in the same boat.

Everyone knows how much the given requirements of a customer can differ from they actual needs. In the end it is to late to change things and much money was spend without any productive outcome. Sad but true.

So BDD should help here with giving the customer the ability to write their acceptance criteria and requirements in a english, well readable formal text. This text can be interpreted by a testing framework like codeception. Yes. I am serious. The customer can write the tests for us and we can be sure that our code fulfills their requirements. Sounds pretty amazing, huh? Because IT IS.

Btw. BDD is a extension of TDD which means that it NEEDS automatic tests to fulfill its full potential. A short reminder what TDD is will be added on this page later. Till then pls read through this: http://www.agilenutshell.com/test_driven_development

## How does it work?



